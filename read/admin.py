from django.contrib import admin
from read.models import bookTbl, chapterTbl
# Register your models here.

admin.site.register(bookTbl)
admin.site.register(chapterTbl)
