from django.urls import path
from .views import *
urlpatterns = [
    path('', index),
    path('read/<int:book>/<int:chapter>', read),
]