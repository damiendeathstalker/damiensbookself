from django.shortcuts import render, HttpResponse
from .models import bookTbl, chapterTbl
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.
def index(request):
	books = bookTbl.objects.all()
	return render(request, "read/index.html", locals())

def read(request, book, chapter):
	book = bookTbl.objects.get(id=book)
	chapters = chapterTbl.objects.filter(book_foreignkey=book).order_by("id")
	try:
		chapter = chapterTbl.objects.get(id=chapter)
	except:
		chapter = chapters[0]
	lastchapter_id = chapters[len(chapters)-1].id
	firstchapter_id = chapters[0].id
	return render(request, "read/read.html", locals())