from django.db import models

# Create your models here.
class bookTbl(models.Model):
    name = models.CharField(max_length=250)
    blurb =models.CharField(max_length=500)

class chapterTbl(models.Model):
    chapter_name = models.CharField(max_length=100)
    chapter_blurb = models.CharField(max_length=500)
    chapter_content = models.TextField()
    book_foreignkey = models.ForeignKey(bookTbl, on_delete=models.CASCADE)